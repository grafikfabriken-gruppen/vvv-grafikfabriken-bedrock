#!/usr/bin/env bash
# Provision WordPress Stable

# fetch the first host as the primary domain. If none is available, generate a default using the site name
DOMAIN=`get_primary_host "${VVV_SITE_NAME}".test`
SITE_TITLE=`get_config_value 'site_title' "${DOMAIN}"`
WP_VERSION=`get_config_value 'wp_version' 'latest'`
WP_TYPE=`get_config_value 'wp_type' "single"`
DB_NAME=`get_config_value 'db_name' "${VVV_SITE_NAME}"`
DB_NAME=${DB_NAME//[\\\/\.\<\>\:\"\'\|\?\!\*-]/}
DB_PREFIX=`get_config_value 'db_prefix' "wp_"`
THEME_NAME=`get_config_value 'theme_name' "grafikfabriken"`
USE_GF_THEME=`get_config_value 'use_gf_theme' true`
ACF_KEY=`get_config_value 'acf_key' ""`
WP_USER=`get_config_value 'wp_user' "remote@grafikfabriken.nu"`
WP_USER_PASS=`get_config_value 'wp_user_pass' "password"`
GITLAB_TOKEN=`get_config_value 'gitlab_token' ""`
PROJECT_REPO=`get_config_value 'project_repo' false`

# Make a database, if we don't already have one
echo -e "\nCreating database '${DB_NAME}' (if it's not already there)"
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO wp@localhost IDENTIFIED BY 'wp';"
echo -e "\n DB operations done.\n\n"



# Nginx Logs
mkdir -p ${VVV_PATH_TO_SITE}/log
touch ${VVV_PATH_TO_SITE}/log/error.log
touch ${VVV_PATH_TO_SITE}/log/access.log

# Install and configure the latest stable version of WordPress

echo ${VVV_PATH_TO_SITE};

if [ ! -d "${VVV_PATH_TO_SITE}/public_html"  ]
then
  if [ "$PROJECT_REPO" != false  ]
  then
    # For existing repos
    git clone ${PROJECT_REPO} public_html
    cd ${VVV_PATH_TO_SITE}/public_html
    composer install
    cd ${VVV_PATH_TO_SITE}/public_html/web/app/themes/${THEME_NAME}
    composer install
    npm i
    npm run build
  
  else
    # For new repos

    cd ${VVV_PATH_TO_SITE}
    
    echo "Installing Bedrock stack using Composer"
    noroot composer create-project grafikfabriken/bedrock public_html

    #edit existing .env file to replace vars
    echo -e "Updating Bedrock .env file with new vars"

    cd ${VVV_PATH_TO_SITE}/public_html
    sed -i.env "s/database_name/${DB_NAME}/g" .env
    sed -i.env "s/database_user/wp/g" .env
    sed -i.env "s/database_password/wp/g" .env
    sed -i.env "s/database_host/localhost/g" .env
    sed -i.env "s/# DB_PREFIX='wp_'/DB_PREFIX='${DB_PREFIX}'/g" .env
    sed -i.env "s/DB_PREFIX='wp_'/DB_PREFIX='${DB_PREFIX}'/g" .env
    sed -i.env "s/example.com/${VVV_SITE_NAME}.test/g" .env

    ##Adding companion
    cd ${VVV_PATH_TO_SITE}/public_html

    noroot composer config repositories.grafikfabriken/companion vcs git@gitlab.com:alexander8/gf-companion.git
    noroot composer config gitlab-token.gitlab.com ${GITLAB_TOKEN}

    if [ "$USE_GF_THEME" = true ] ; then

      echo "Installing gf-theme using Composer"
      noroot composer create-project grafikfabriken/theme web/app/themes/${THEME_NAME}

      # Append rules for wp-cli so that "wp rewrite flush --hard" works
      echo >> wp-cli.yml && echo "apache_modules:" >> wp-cli.yml && echo "  - mod_rewrite" >> wp-cli.yml;

      cd ${VVV_PATH_TO_SITE}/public_html/web/app/themes/

      if [ -d "${THEME_NAME}" ]
      then
        echo "Changeing theme name"
        sed -i "s/Theme Name: Grafikfabriken starter theme/Theme Name: ${THEME_NAME}/g" "${VVV_PATH_TO_SITE}/public_html/web/app/themes/${THEME_NAME}/app/style.css"
      fi
    fi

    
    cd ${VVV_PATH_TO_SITE}/public_html

    if ! $(noroot wp core is-installed); then
      echo "Installing WordPress Stable..."

      if [ "${WP_TYPE}" = "subdomain" ]; then
        INSTALL_COMMAND="multisite-install --subdomains"
      elif [ "${WP_TYPE}" = "subdirectory" ]; then
        INSTALL_COMMAND="multisite-install"
      else
        INSTALL_COMMAND="install"
      fi


      noroot wp core ${INSTALL_COMMAND} --url="${DOMAIN}" --quiet --title="${SITE_TITLE}" --admin_name=${WP_USER} --admin_email=${WP_USER} --admin_password=${WP_USER_PASS}
      echo "Login with: ${WP_USER}/${WP_USER_PASS}"

    fi

    if [ ! -z "$ACF_KEY"  ]
    then

      echo "Installing acf-pro using WGet"

      # get plugin path
      acf_zip_file="${VVV_PATH_TO_SITE}/public_html/web/app/plugins/acf-pro.zip"

      # get acf zip file
      wget -O ${acf_zip_file} "http://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=${ACF_KEY}"

      cd ${VVV_PATH_TO_SITE}/public_html
      
      echo "Adding ACF-Pro to .gitignore"
      echo "" >> "${VVV_PATH_TO_SITE}/public_html/.gitignore"
      echo "#Addons GF" >> "${VVV_PATH_TO_SITE}/public_html/.gitignore"
      echo "!web/app/plugins/advanced-custom-fields-pro" >> "${VVV_PATH_TO_SITE}/public_html/.gitignore"

      # install & activate acf
      noroot wp plugin install ${acf_zip_file} --activate



      # remove zip file
      rm ${acf_zip_file}
    fi

    if [ ! -z "$GITLAB_TOKEN"  ]
    then

      cd ${VVV_PATH_TO_SITE}/public_html

      echo "Activate GF-companion"
      noroot wp plugin activate gf-companion

      echo "Activate ${THEME_NAME}-theme"
      noroot wp theme activate ${THEME_NAME}/app

      echo "Install swedish core"
      noroot wp language core install sv_SE

      noroot wp site switch-language sv_SE

      echo "Create defualt rewrite rules"
      noroot wp rewrite structure '/%postname%/'

      echo "Flush rewrite rules"
      noroot wp rewrite flush

      if [ "$USE_GF_THEME" = true ] ; then
        echo "Build styles"
        cd ${VVV_PATH_TO_SITE}/public_html/web/app/themes/${THEME_NAME}/
        noroot npm install && npm run build
      fi

    fi
  fi
fi


cp -f "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf.tmpl" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"

sed -i "s#{{TLS_CERT}}#ssl_certificate /srv/certificates/${VVV_SITE_NAME}/dev.crt;#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
    sed -i "s#{{TLS_KEY}}#ssl_certificate_key /srv/certificates/${VVV_SITE_NAME}/dev.key;#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"

# if [ -n "$(type -t is_utility_installed)" ] && [ "$(type -t is_utility_installed)" = function ] && `is_utility_installed core tls-ca`; then
#     sed -i "s#{{TLS_CERT}}#ssl_certificate /srv/certificates/${VVV_SITE_NAME}/dev.crt;#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
#     sed -i "s#{{TLS_KEY}}#ssl_certificate_key /srv/certificates/${VVV_SITE_NAME}/dev.key;#" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
# else
#     sed -i "s#{{TLS_CERT}}##" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
#     sed -i "s#{{TLS_KEY}}##" "${VVV_PATH_TO_SITE}/provision/vvv-nginx.conf"
# fi
