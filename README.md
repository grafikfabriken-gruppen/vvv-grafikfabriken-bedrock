# VVV Grafikfabriken Bedrock

For when you just need a simple dev site

## Overview

This template will allow you to create a WordPress dev environment using only `vvv-custom.yml`.

The supported environments are:

-   A single site
-   A subdomain multisite
-   A subdirectory multisite

# Configuration

### The minimum required configuration:

```
sms:
    description: "sms WP Site"
    repo: https://gitlab.com/grafikfabriken-gruppen/vvv-grafikfabriken-bedrock
    upstream: php72
    hosts:
      - sms.test
    custom:
      site_title: "sms"
      db_name: "sms_dev"
      db_prefix: "sms_"
      theme_name: "sms"
      acf_key: "your_key"
      wp_user: "username"
      wp_user_pass: "password"
      gitlab_token: "token"
```

### WordPress Multisite with Subdomains:

```
my-site:
  repo: https://github.com/Varying-Vagrant-Vagrants/custom-site-template
  hosts:
    - multisite.test
    - site1.multisite.test
    - site2.multisite.test
  custom:
    wp_type: subdomain
```

| Setting    | Value               |
| ---------- | ------------------- |
| Domain     | multisite.test      |
| Site Title | multisite.test      |
| DB Name    | my-site             |
| Site Type  | Subdomain Multisite |

### WordPress Multisite with Subdirectory:

```
my-site:
  repo: https://github.com/Varying-Vagrant-Vagrants/custom-site-template
  hosts:
    - multisite.test
  custom:
    wp_type: subdirectory
```

| Setting    | Value                  |
| ---------- | ---------------------- |
| Domain     | multisite.test         |
| Site Title | multisite.test         |
| DB Name    | my-site                |
| Site Type  | Subdirectory Multisite |
